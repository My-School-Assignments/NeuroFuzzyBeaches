import math
import random

for i in range(0, 5):
    with open('situations%d.csv' % i, 'w') as file:
        for j in range(0, 500):
            distanceLeft = random.randint(15, 750)
            if distanceLeft > 620:
                distanceLeft = -1

            distanceRight = random.randint(15, 750)
            if distanceRight > 620:
                distanceRight = -1

            distanceFront = random.randint(15, 750)
            if distanceFront > 620:
                distanceFront = -1

            targetX = random.randint(-1000, 1000)
            targetY = random.randint(-1000, 1000)
            angleTarget = math.degrees(math.atan2(targetX, targetY))

            file.write('%d,%d,%d,%d,  #Target: %d, %d\n' % (distanceLeft, distanceRight, distanceFront, angleTarget,
                                                            targetX, targetY))

print('Done!\n')