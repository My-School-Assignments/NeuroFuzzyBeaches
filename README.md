# NeuroFuzzy Beaches
This project was created as school assignment for Hybrid Computational Intelligence class at Technical University of Kosice

[**Presentation Website**](https://neurofuzzy.azurewebsites.net/) 

---
© Dominik Horňák, Ivan Vojtko, Igor Marga, Štefan Labbancz, Richard Halčin (2018)