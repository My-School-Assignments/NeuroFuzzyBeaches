#include <HCSR04.h>

UltraSonicDistanceSensor distanceSensor0(11, 10);
UltraSonicDistanceSensor distanceSensor1(9, 8);
UltraSonicDistanceSensor distanceSensor2(7, 6);

void setup () {
    Serial.begin(9600);
}

void loop () {
    Serial.print("0 ");
    Serial.print(distanceSensor0.measureDistanceCm());
    Serial.print(" 1 ");
    Serial.print(distanceSensor1.measureDistanceCm());
    Serial.print(" 2 ");
    Serial.print(distanceSensor2.measureDistanceCm());
    delay(500);
}
