import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
#import matplotlib.pyplot as plt

class Fuzzicek():
    system = None
    def __init__(self):
        ####### MEMBERSHIP FUNCTIONS
        x_front = np.arange(0, 4, 0.01)
        x_left = np.arange(0, 4, 0.01)
        x_right = np.arange(0, 4, 0.01)
        x_head = np.arange(-180, 180, 1)
        x_velocity = np.arange(0, 0.7, 0.01)

        ###### FUZZY RULES
        leftObs = ctrl.Antecedent(x_left, 'leftObs')
        frontObs = ctrl.Antecedent(x_front, 'frontObs')
        rightObs = ctrl.Antecedent(x_right, 'rightObs')
        headAng = ctrl.Antecedent(x_head, 'headAng')
        leftVelo = ctrl.Consequent(x_velocity, 'leftVelo')
        rightVelo = ctrl.Consequent(x_velocity, 'rightVelo')

        namesObs = ['Far', 'Med', 'Near']
        namesAng = ['Neg', 'Z', 'Pos']
        namesVelo = ['Slow', 'Med', 'Fast']

        leftObs.automf(names=namesObs)
        frontObs.automf(names=namesObs)
        rightObs.automf(names=namesObs)
        headAng.automf(names=namesAng)
        leftVelo.automf(names=namesVelo)
        rightVelo.automf(names=namesVelo)

        # front sensor
        frontObs['Near'] = fuzz.trapmf(x_front, [0, 0, 0.3, 1.0])
        frontObs['Med'] = fuzz.trimf(x_front, [0.3, 1.0, 3.4])
        frontObs['Far'] = fuzz.trapmf(x_front, [1.0, 3.4, 4.0,4.0])
        # left sensor
        leftObs['Near'] = fuzz.trapmf(x_left, [0, 0, 0.4, 1.0])
        leftObs['Med'] = fuzz.trimf(x_left, [0.4, 1.0, 3.2])
        leftObs['Far'] = fuzz.trapmf(x_left, [1.0, 3.2, 4.0,4.0])
        # right sensor
        rightObs['Near'] = fuzz.trapmf(x_right, [0, 0, 0.4, 1.0])
        rightObs['Med'] = fuzz.trimf(x_right, [0.4, 1.0, 3.2])
        rightObs['Far'] = fuzz.trapmf(x_right, [1.0, 3.2, 4.0,4.0])
        # Heading angle
        headAng['Neg'] = fuzz.trapmf(x_head, [-180, -180, -20, 0])
        headAng['Z'] = fuzz.trimf(x_head, [-20, 0, 20])
        headAng['Pos'] = fuzz.trapmf(x_head, [0, 20, 180,180])
        # Left velocity
        leftVelo['Slow'] = fuzz.trapmf(x_velocity, [0.0, 0.0, 0.25, 0.35])
        leftVelo['Med'] = fuzz.trimf(x_velocity, [0.25, 0.35, 0.45])
        leftVelo['Fast'] = fuzz.trapmf(x_velocity, [0.35, 0.45, 0.7,0.7])
        # Right velocity
        rightVelo['Slow'] = fuzz.trapmf(x_velocity, [0.0, 0.0, 0.25, 0.35])
        rightVelo['Med'] = fuzz.trimf(x_velocity, [0.25, 0.35, 0.45])
        rightVelo['Fast'] = fuzz.trapmf(x_velocity, [0.35, 0.45, 0.7,0.7])

        # Obstacle avoidance
        rule0 = ctrl.Rule(((leftObs['Far'] & frontObs['Near'] & rightObs['Med'] & headAng['Neg'])),
        (leftVelo['Slow'], rightVelo['Fast']))

        rule1 = ctrl.Rule(((leftObs['Far'] & frontObs['Near'] & rightObs['Far'] & headAng['Z'])),
        (leftVelo['Fast'], rightVelo['Slow']))

        rule2 = ctrl.Rule(((leftObs['Far'] & frontObs['Near'] & rightObs['Near'] & headAng['Neg'])),
        (leftVelo['Slow'], rightVelo['Fast']))

        rule3 = ctrl.Rule(((leftObs['Med'] & frontObs['Med'] & rightObs['Near'] & headAng['Neg'])),
        (leftVelo['Slow'], rightVelo['Fast']))

        rule4 = ctrl.Rule(((leftObs['Med'] & frontObs['Near'] & rightObs['Far'] & headAng['Pos'])),
        (leftVelo['Fast'], rightVelo['Slow']))

        rule5 = ctrl.Rule(((leftObs['Med'] & frontObs['Near'] & rightObs['Med'] & headAng['Neg'])),
        (leftVelo['Slow'], rightVelo['Fast']))

        rule6 = ctrl.Rule(((leftObs['Med'] & frontObs['Near'] & rightObs['Near'] & headAng['Neg'])),
        (leftVelo['Slow'], rightVelo['Fast']))

        rule7 = ctrl.Rule(((leftObs['Near'] & frontObs['Med'] & rightObs['Med'] & headAng['Z'])),
        (leftVelo['Med'], rightVelo['Med']))

        rule8 = ctrl.Rule(((leftObs['Near'] & frontObs['Med'] & rightObs['Near'] & headAng['Z'])),
        (leftVelo['Med'], rightVelo['Med']))

        rule9 = ctrl.Rule(((leftObs['Near'] & frontObs['Near'] & rightObs['Far'] & headAng['Pos'])),
        (leftVelo['Fast'], rightVelo['Slow']))

        rule10 = ctrl.Rule(((leftObs['Near'] & frontObs['Near'] & rightObs['Med'] & headAng['Pos'])),
        (leftVelo['Fast'], rightVelo['Slow']))

        rule11 = ctrl.Rule(((leftObs['Near'] & frontObs['Near'] & rightObs['Near'] & headAng['Pos'])),
        (leftVelo['Fast'], rightVelo['Slow']))

        # Obstacle avoidance and wall following
        rule12 = ctrl.Rule(((leftObs['Far'] & frontObs['Far'] & rightObs['Near'] & headAng['Z'])),
        (leftVelo['Fast'], rightVelo['Fast']))

        rule13 = ctrl.Rule(((leftObs['Far'] & frontObs['Med'] & rightObs['Near'] & headAng['Z'])),
        (leftVelo['Med'], rightVelo['Med']))

        rule14 = ctrl.Rule(((leftObs['Med'] & frontObs['Far'] & rightObs['Near'] & headAng['Z'])),
        (leftVelo['Med'], rightVelo['Med']))

        rule15 = ctrl.Rule(((leftObs['Near'] & frontObs['Far'] & rightObs['Med'] & headAng['Z'])),
        (leftVelo['Med'], rightVelo['Med']))

        rule16 = ctrl.Rule(((leftObs['Near'] & frontObs['Far'] & rightObs['Near'] & headAng['Z'])),
        (leftVelo['Slow'], rightVelo['Slow']))

        rule17 = ctrl.Rule(((leftObs['Near'] & frontObs['Med'] & rightObs['Far'] & headAng['Z'])),
        (leftVelo['Med'], rightVelo['Med']))

        # Target finding
        rule18 = ctrl.Rule(((leftObs['Far'] & frontObs['Far'] & rightObs['Far'] & headAng['Pos'])),
        (leftVelo['Fast'], rightVelo['Slow']))

        rule19 = ctrl.Rule(((leftObs['Far'] & frontObs['Far'] & rightObs['Far'] & headAng['Neg'])),
        (leftVelo['Slow'], rightVelo['Fast']))

        rule20 = ctrl.Rule(((leftObs['Far'] & frontObs['Far'] & rightObs['Near'] & headAng['Neg'])),
        (leftVelo['Slow'], rightVelo['Fast']))

        rule21 = ctrl.Rule(((leftObs['Near'] & frontObs['Far'] & rightObs['Far'] & headAng['Pos'])),
        (leftVelo['Fast'], rightVelo['Slow']))

        rule22 = ctrl.Rule(((leftObs['Far'] & frontObs['Near'] & rightObs['Near'] & headAng['Neg'])),
        (leftVelo['Slow'], rightVelo['Fast']))

        rule23 = ctrl.Rule(((leftObs['Near'] & frontObs['Near'] & rightObs['Far'] & headAng['Pos'])),
        (leftVelo['Fast'], rightVelo['Slow']))

        system_ctrl = ctrl.ControlSystem(rules=[rule0, rule1, rule2, rule3, rule4,
        rule5, rule6, rule7, rule8, rule9, rule10, rule11, rule12, rule13, rule14,
        rule15, rule16, rule17, rule18, rule19, rule20, rule21, rule22, rule23])
        self.system = ctrl.ControlSystemSimulation(system_ctrl)

    def DestroyTheWorld(self, left, right, front, ang):
        try:
            self.system.input['leftObs'] = left
            self.system.input['rightObs'] = right
            self.system.input['frontObs'] = front
            self.system.input['headAng'] = ang
    	    #print(str(left)+" "+str(right)+" "+str(front)+" "+str(ang))
            self.system.compute()
            #print(self.system.output['leftVelo'])
            #print(self.system.output['rightVelo'])
            return self.system.output['leftVelo'], self.system.output['rightVelo']
        except:
            return 0.0, 0.0


# def plotMembershipFunctions():
#     fig, (ax0, ax1, ax2, ax3, ax4, ax5) = plt.subplots(nrows=6, figsize=(8, 9))
#     ax0.plot(x_front, front_near, 'b', linewidth=1.5, label='Near')
#     ax0.plot(x_front, front_med, 'g', linewidth=1.5, label='Medium')
#     ax0.plot(x_front, front_far, 'r', linewidth=1.5, label='Far')
#     ax0.set_title('Front sensor')
#     ax0.legend()
#
#     ax1.plot(x_left, left_near, 'b', linewidth=1.5, label='Near')
#     ax1.plot(x_left, left_med, 'g', linewidth=1.5, label='Medium')
#     ax1.plot(x_left, left_far, 'r', linewidth=1.5, label='Far')
#     ax1.set_title('Left sensor')
#     ax1.legend()
#     ax2.plot(x_right, right_near, 'b', linewidth=1.5, label='Near')
#     ax2.plot(x_right, right_med, 'g', linewidth=1.5, label='Medium')
#     ax2.plot(x_right, right_far, 'r', linewidth=1.5, label='Far')
#     ax2.set_title('Right sensor')
#     ax2.legend()
#
#     ax3.plot(x_head, head_neg, 'b', linewidth=1.5, label='Negative')
#     ax3.plot(x_head, head_zero, 'g', linewidth=1.5, label='Zero')
#     ax3.plot(x_head, head_pos, 'r', linewidth=1.5, label='Positive')
#     ax3.set_title('Heading angle')
#     ax3.legend()
#
#     ax4.plot(x_velocity, leftVel_slow, 'b', linewidth=1.5, label='Slow')
#     ax4.plot(x_velocity, leftVel_med, 'g', linewidth=1.5, label='Medium')
#     ax4.plot(x_velocity, leftVel_fast, 'r', linewidth=1.5, label='Fast')
#     ax4.set_title('Left velocity')
#     ax4.legend()
#
#     ax5.plot(x_velocity, leftVel_slow, 'b', linewidth=1.5, label='Slow')
#     ax5.plot(x_velocity, leftVel_med, 'g', linewidth=1.5, label='Medium')
#     ax5.plot(x_velocity, leftVel_fast, 'r', linewidth=1.5, label='Fast')
#     ax5.set_title('Right velocity')
#     ax5.legend()
#     # Turn off top/right axes
#     for ax in (ax0, ax1, ax2, ax3, ax4, ax5):
#         ax.spines['top'].set_visible(False)
#         ax.spines['right'].set_visible(False)
#         ax.get_xaxis().tick_bottom()
#         ax.get_yaxis().tick_left()
#     plt.tight_layout()
#     plt.show()
#
# plotMembershipFunctions()

