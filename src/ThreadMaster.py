import threading
import rospy
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
import serial
import math

import Neuro
import fuzzicek


class ThreadMaster:
    def __init__(self):
        print('Init start')
        self.HWData = None
        self.OdoData = None
        self.OldOdometry = [0.0, 0.0, 0.0]  # X, Y, TH
        self.VelocityLeft = 0.0
        self.VelocityRight = 0.0
        self.Target = [0.0, 0.0]  # X, Y

        self.nn = Neuro.NeuroNetto()
        self.fuzzy = fuzzicek.Fuzzicek()

        port = '/dev/ttyUSB1'
        baud = 9600

        self.igormarga = None

        serial_port = serial.Serial(port, baud, timeout=0)

        def handle_data(data):
            if self.HWData is None and len(data) > 2 and data.replace(' ', '')[0] == '0':
                # print('Handle Data ' + str(data))
                self.igormarga = data
            elif self.igormarga is not None and self.HWData is None:
                self.igormarga = self.igormarga + data

        if self.igormarga is not None:
            d = self.igormarga.split(' ')
            if len(d) >= 6:
                self.igormarga = None
                self.HWData = [d[2], d[4], d[6]]  # 1 - Left, 2 - Right, 3 - Front
        # print(self.igormarga)

        def read_from_port(ser):
            while True:
                try:
                    # print('Read from Port')
                    reading = ser.readline().decode()
                    handle_data(reading)
                    # print('Read from port')
                except:
                    print('Bordel')
                    self.HWData = None
                    self.OdoData = None
                    self.OldOdometry = [0.0, 0.0, 0.0]  # X, Y, TH
                    self.VelocityLeft = 0.0
                    self.VelocityRight = 0.0

        threadHW = threading.Thread(target=read_from_port, args=(serial_port,))
        threadHW.start()

        def to_positive_angle(th):
            while th <= -180:
                th += 360
            while th > 180:
                th -= 360
            return th

        def suck_my_d():
            while True:
                # print(str(self.HWData)+" "+str(self.OdoData))
                if self.HWData is not None and self.OdoData is not None:
                    # print(self.HWData)
                    if self.HWData[0] == '':
                        self.HWData[0] = '30.0'
                    if self.HWData[1] == '':
                        self.HWData[1] = '30.0'
                    if self.HWData[2] == '':
                        self.HWData[2] = '30.0'

                    pred_x = {
                                'DistanceLeft': [float(self.HWData[0])],
                                'DistanceRight': [float(self.HWData[1])],
                                'DistanceFront': [float(self.HWData[2])],
                                'TargetAngle': [float(self.OdoData)],
                            }
                    # print(pred_x)

                    nnOut = to_positive_angle(self.nn.predict(pred_x))
                    # print('NN...' + str(nn))
                    
                    if float(self.HWData[0]) == -1.0:
                        self.HWData[0] = 4
                    if float(self.HWData[1]) == -1.0:
                        self.HWData[1] = 4
                    if float(self.HWData[2]) == -1.0:
                        self.HWData[2] = 4

                    self.VelocityLeft, self.VelocityRight = \
                        self.fuzzy.DestroyTheWorld(float(self.HWData[0])/100.0, float(self.HWData[1])/100.0, float(self.HWData[2])/100.0, nnOut)

                    # print(str(self.VelocityLeft) + ' ' + str(self.VelocityRight))
                    self.HWData = None
                    self.OdoData = None

        threadAI = threading.Thread(target=suck_my_d, args=())
        threadAI.start()

        def odo(data):
            # print('Odo......')
            if self.OdoData is None:
                # print('Odo...')
                y = data.pose.pose.position.x*100.0  # Forward
                x = data.pose.pose.position.y*100.0  # Right
                q1 = data.pose.pose.orientation.x
                q2 = data.pose.pose.orientation.y
                q3 = data.pose.pose.orientation.z
                q4 = data.pose.pose.orientation.w
                q = (q1, q2, q3, q4)
                e = euler_from_quaternion(q)
                th = math.degrees(e[2])
                th = to_positive_angle(th)

                newTH = math.radians(th - self.OldOdometry[2])
                self.OldOdometry[2] = th

                newX = x - self.OldOdometry[0]
                self.OldOdometry[0] = x

                newY = y - self.OldOdometry[1]
                self.OldOdometry[1] = y

                newTargetX = self.Target[0] * math.cos(newTH) - self.Target[1] * math.sin(newTH)
                newTargetY = self.Target[1] * math.cos(newTH) + self.Target[0] * math.sin(newTH)
                newTargetX += newX
                newTargetY += newY

                self.Target[0] = -newTargetX
                self.Target[1] = newTargetY

                self.OdoData = to_positive_angle(math.degrees(math.atan2(self.Target[0], self.Target[1])))
                # print(self.OdoData)

        sub = rospy.Subscriber('/odom', Odometry, odo)
