# Neural Network - Load model and Predict

import tensorflow
import os


NORM_FACTOR_DIST = 620.0
NORM_FACTOR_ANGLE = 180.0
FEATURE_COLS = ['DistanceLeft', 'DistanceRight', 'DistanceFront', 'TargetAngle']


class NeuroNetto:
    def __init__(self):
        my_feature_columns = []
        for key in FEATURE_COLS:
            my_feature_columns.append(tensorflow.feature_column.numeric_column(key=key))

        self.regressor = tensorflow.estimator.DNNRegressor(
            feature_columns=my_feature_columns,
            hidden_units=[10, 3],
            model_dir=os.path.dirname(os.path.realpath(__file__)) + '/../Models/RC1/')

    def predict(self, pred_x):
        pred_x['DistanceLeft'][:] = [x / NORM_FACTOR_DIST for x in pred_x['DistanceLeft']]
        pred_x['DistanceRight'][:] = [x / NORM_FACTOR_DIST for x in pred_x['DistanceRight']]
        pred_x['DistanceFront'][:] = [x / NORM_FACTOR_DIST for x in pred_x['DistanceFront']]
        pred_x['TargetAngle'][:] = [x / NORM_FACTOR_ANGLE for x in pred_x['TargetAngle']]

        preds = self.regressor.predict(input_fn=lambda: self.eval_input_fn(pred_x, 30))
        outAngle = 0

        for i, prediction in enumerate(preds):
            outAngle = prediction["predictions"][0] * NORM_FACTOR_ANGLE
            #print(str(outAngle))

        return outAngle

    def eval_input_fn(self, features, batch_size):
        inputs = dict(features)

        dataset = tensorflow.data.Dataset.from_tensor_slices(inputs)
        dataset = dataset.batch(batch_size)

        return dataset
