import pandas as pd
import tensorflow as tf

CSV_COLUMN_NAMES = ['DistanceLeft', 'DistanceRight',
                    'DistanceFront', 'TargetAngle', 'OutputAngle']
NORM_FACTOR_DIST = 620.0
NORM_FACTOR_ANGLE = 180.0


def filter(path):
    raw = pd.read_csv(path, names=CSV_COLUMN_NAMES, header=0)
    ta = raw['TargetAngle']
    oa = raw['OutputAngle']
    aa = ta - oa
    filtered = raw[aa.abs() > 20.0]
    filtered.to_csv(path + "_new", index=False)


def filter2(path):
    raw = pd.read_csv(path, names=CSV_COLUMN_NAMES, header=0)
    # ta = raw['TargetAngle']
    # oa = raw['OutputAngle']
    # aa = ta - oa
    filtered = raw[(raw.DistanceFront < 60.0) & (raw.DistanceFront >= 0.0)]
    filtered.to_csv(path + "_new3", index=False)


def load_data(path):
    raw = pd.read_csv(path, names=CSV_COLUMN_NAMES, header=0)

    raw.DistanceLeft = raw.DistanceLeft / NORM_FACTOR_DIST
    raw.DistanceRight = raw.DistanceRight / NORM_FACTOR_DIST
    raw.DistanceFront = raw.DistanceFront / NORM_FACTOR_DIST
    raw.TargetAngle = raw.TargetAngle / NORM_FACTOR_ANGLE
    raw.OutputAngle = raw.OutputAngle / NORM_FACTOR_ANGLE

    train = raw.sample(frac=0.6, random_state=200)
    train_x, train_y = train, train.pop('OutputAngle')

    test = raw.drop(train.index)
    test_x, test_y = test, test.pop('OutputAngle')

    return (train_x, train_y), (test_x, test_y)


def train_input_fn(features, labels, batch_size):
    dataset = tf.data.Dataset.from_tensor_slices((dict(features), labels))
    dataset = dataset.shuffle(3000).repeat().batch(batch_size)

    return dataset


def eval_input_fn(feat, labels, batch_size):
    features = dict(feat)

    if labels is None:
        inputs = features
    else:
        inputs = (features, labels)

    dataset = tf.data.Dataset.from_tensor_slices(inputs)
    dataset = dataset.batch(batch_size)

    return dataset
