import tensorflow as tf
import NeuralTraining.DataLoader


class NeuralNet:
    def __init__(self, path):
        (self.train_x, self.train_y), (self.test_x, self.test_y) = NeuralTraining.DataLoader.load_data(path)

        my_feature_columns = []
        for key in self.train_x.keys():
            my_feature_columns.append(tf.feature_column.numeric_column(key=key))

        self.regressor = tf.estimator.DNNRegressor(
            feature_columns=my_feature_columns,
            hidden_units=[10, 3],
        )

        self.regressor.train(input_fn=lambda: NeuralTraining.DataLoader.train_input_fn(self.train_x, self.train_y, 30),
                             steps=50000)

        eval_result = self.regressor.evaluate(input_fn=lambda: NeuralTraining.DataLoader.eval_input_fn(self.test_x, self.test_y, 30))

        print('\nTest accuracy: ' + str(eval_result))

        pred_x = {
            'DistanceLeft': [35 / 620.0, -1 / 620.0, 35 / 620.0, -1 / 180.0],
            'DistanceRight': [430 / 620.0, 135 / 620.0, 35 / 620.0, -1 / 180.0],
            'DistanceFront': [125 / 620.0, 50 / 620.0, 35 / 620.0, 23 / 180.0],
            'TargetAngle': [35 / 620.0, -75 / 620.0, -10 / 620.0, -1 / 180.0],
        }

        preds = self.regressor.predict(input_fn=lambda: NeuralTraining.DataLoader.eval_input_fn(pred_x, None, 30))
        for i, prediction in enumerate(preds):
            msg = str(prediction["predictions"][0] * 180.0)
            print("    " + msg)
