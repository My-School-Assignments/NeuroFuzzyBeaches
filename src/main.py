#!/usr/bin/env python
import ThreadMaster
import rospy
import time
import math
import rosVelociraptor
from geometry_msgs.msg import Twist

print('Start...')
rospy.init_node("CoffeeKiller")
print('Ros Init...')
teleop_pub = rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=5)
controler = rosVelociraptor.RobotControl(teleop_pub)
print('Velociraptor')
master = ThreadMaster.ThreadMaster()
print('ThreadMaster....')

r = rospy.Rate(1)

while not rospy.is_shutdown():
    #controler.spinWheels(5, 10, 0.01)
    if math.sqrt(master.Target[0] * master.Target[0] + master.Target[1] * master.Target[1]) > 50:
	print(str(master.Target[0]) + " " + str(master.Target[1]))
        #print('spin wheels... ' + str(master.VelocityRight)+ ' ' + str(master.VelocityLeft))
        if master.VelocityRight * 20.0 < 0.001:
            master.VelocityRight = -0.0005
	if master.VelocityLeft * 20.0 < 0.001:
	    master.VelocityLeft = -0.0005
        controler.spinWheels(master.VelocityRight*20.0, master.VelocityLeft*20.0, 1)
    else:
        controler.stop()
        print('Turtelbot at target location! Please set new target location (bot is at [0, 0]):')
        x = input('Insert X (in cm): ')
        y = input('Insert Y (in cm): ')

        master.Target[0] = x
        master.Target[1] = y

    r.sleep()
